<?php
function checkCollision($x, $y, $r) {
	/**
	 * Фунция проверяет пападает ли точка (x, y) в заданную область.
	 */

	if ($x > 0 && $y >= 0) {
		return 4 * ($x * $x + $y * $y) <= $r * $r;
	}
	else if ($x <= 0 && $y > 0) {
		return $y <= 2 * $x + $r;
	}
	else if ($x <= 0 && $y <= 0) {
		return $x >= -$r && $y >= -$r;
	}
	else {
		return false;
	}
}

function validateNumber($number, $min, $max) {
	/**
	 * Функция проверяет является ли $number числом и попадает ли оно в диапазон [$min; $max].
	 */

	return isset($number) && is_numeric($number) && 
			$number >= $min && $number <= $max;
}

function validateInput($x, $y, $r) {
	/**
	 * Функция проверяет полученные значения $x, $y, $r.
	 */

	return  validateNumber($x, -5, 3) &&
			validateNumber($y, -5, 3) &&
			validateNumber($r, 1, 5);
}


session_start();

if ($_SERVER['REQUEST_METHOD'] != "POST") {
	http_response_code(405);
	return;
}

// Если истории нету, то создаем новую
if (!isset($_SESSION["data"])) {
    $_SESSION["data"] = array();
}

// Если длина истории больше 14, то обрезаем
if (count($_SESSION["data"]) > 14) {
	array_splice($_SESSION["data"], 14 - count($_SESSION["data"]));
}

$x = isset($_POST["x-input"]) ? str_replace(",", ".", $_POST["x-input"]) : null;
$y = isset($_POST["y-input"]) ? $_POST["y-input"] : null;
$r = isset($_POST["r-input"]) ? $_POST["r-input"] : null;

if (!validateInput($x, $y, $r)) {
	http_response_code(400);
	return;
}

date_default_timezone_set($_SESSION['timezone']);

// Если полученные значения корректныи, то формируем новую строку таблицы
$current_time = date("H:i:s", time());
$hit = checkCollision($x, $y, $r);
$execution_time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
$execution_time = round($execution_time * 1000, 3);

$result = [
	"x"=>$x, 
	"y"=>$y, 
	"r"=>$r, 
	"hit"=>$hit, 
	"current_time"=>$current_time, 
	"execution_time"=>$execution_time
];

array_unshift($_SESSION['data'], $result);

// Формируем строку таблицы
echo $result['hit'] ? "<tr class='hit'>" : "<tr class='miss'>";
echo "<td>" . $result['current_time']  . "</td>";
echo "<td>" . $result['execution_time'] . " ms </td>";
echo "<td>" . $result['x'] . "</td>";
echo "<td>" . $result['y'] . "</td>";
echo "<td>" . $result['r'] . "</td>";
echo $result['hit'] ? "<td>Попадание</td>" : "<td>Промах</td>";
echo "</tr>";
?>
