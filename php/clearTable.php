<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] != "POST") {
	http_response_code(405);
	return;
}

$_SESSION["data"] = array();
?>