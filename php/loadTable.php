<?php
session_start();



if ($_SERVER['REQUEST_METHOD'] != "GET") {
	http_response_code(405);
	return;
}

$_SESSION['timezone'] = $_GET['timezone'];

if (!isset($_SESSION["data"])) {
    $_SESSION["data"] = array();
}

foreach ($_SESSION["data"] as $row) {
	echo $row['hit'] ? "<tr class='hit'>" : "<tr class='miss'>";
	echo "<td>" . $row['current_time']  . "</td>";
	echo "<td>" . $row['execution_time'] . " ms </td>";
	echo "<td>" . $row['x'] . "</td>";
	echo "<td>" . $row['y'] . "</td>";
	echo "<td>" . $row['r'] . "</td>";
	echo $row['hit'] ? "<td>Попадание</td>" : "<td>Промах</td>";
	echo "</tr>";
}
?>