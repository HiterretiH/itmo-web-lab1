const GRAPH_SIZE = 300;
const R_SIZE = 100;
const form = document.getElementById("input-form");

$("#input-form").bind("submit", submitForm);
$("#input-form").bind("input", updateArea);
$("#x-input").bind("input", validateX);

$("#x-input").bind("input", function(event) {sessionStorage.setItem("x-input", event.target.value)});
$("input[name=y]").bind("input", function(event) {sessionStorage.setItem("y-input", event.target.value)});
$("#r-input").bind("input", function(event) {sessionStorage.setItem("r-input", event.target.value)});

$("#clear-button").on("click", function() {
  $.ajax({
    url: 'php/clearTable.php',
    method: "POST",
    success: function(data){
      $("#result-table-body").html(data);
    },
    error: function(error){
      console.log(error);	
    },
  });
});

$(document).ready(function() {
  $.ajax({
    url: 'php/loadTable.php',
    method: "GET",
    data: "timezone=" + Intl.DateTimeFormat().resolvedOptions().timeZone,
    success: function(data){
      $("#result-table-body").html(data);
    },
    error: function(error){
      console.log(error);	
    },
  });

  if (sessionStorage.getItem("x-input")) {
    $("#x-input").val(sessionStorage.getItem("x-input"));
  }
  if (sessionStorage.getItem("y-input")) {
    $("input[name=y][value=" + sessionStorage.getItem("y-input") + "]").prop("checked", true);
  }
  if (sessionStorage.getItem("r-input")) {
    $("#r-input").val(sessionStorage.getItem("r-input"));
  }

  updateArea();
});

function updateArea() {
  let x = form.x.value.trim().replace(",", "."),
      y = form.y.value,
      r = form.r.value;
  
  $("#preview-dot").attr({
    cx: x * R_SIZE / r + GRAPH_SIZE / 2, 
    cy: GRAPH_SIZE / 2 - y * R_SIZE / r,
    visibility: "visible"
  });

  $(".r-text").text(r);
  $(".r-2-text").text(r/2);
}

function validateX(event) {
  x_input = event.target;
  x_text = x_input.value;
  x_text = x_text.trim().replace(",", ".");
  
  if (x_text === "") {
    x_input.setCustomValidity("Заполните поле");
  }
  else if (isNaN(x_text) || isNaN(Number.parseFloat(x_text))) {
    x_input.setCustomValidity("X должен быть числом")
  }
  else if (x_text < -5) {
    x_input.setCustomValidity("X должен быть не меньше -5")
  }
  else if (x_text > 3) {
    x_input.setCustomValidity("X должен быть не больше 3")
  }
  else {
    x_input.setCustomValidity("");
  }
}

function submitForm(event) {
  event.preventDefault();

  let x = event.target.x.value.trim().replace(",", "."),
      y = event.target.y.value,
      r = event.target.r.value;

  $.ajax({
    url: 'php/addRow.php',
    method: "POST",
    data: {"x-input": x, "y-input": y, "r-input": r},
    success: function(data){
      $("#result-table-body").prepend(data);
    },
    error: function(error){
      console.log(error);	
    },
  });

  return false;
}
